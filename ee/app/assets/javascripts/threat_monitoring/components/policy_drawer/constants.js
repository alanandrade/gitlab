import { s__ } from '~/locale';

export const ENABLED_LABEL = s__('SecurityOrchestration|Enabled');

export const NOT_ENABLED_LABEL = s__('SecurityOrchestration|Not enabled');
